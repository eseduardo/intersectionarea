# Area de intersección 


Enunciado 

Dos rectángulos A y B, cuyos lados son paralelos a los ejes X y Y, pueden encontrarse en cualquiera
de los tres casos:

* Caso 1: Los rectángulos A y B se superponen.
* Caso 2: Los rectángulos A y B se tocan.
* Case 3: Los rectángulos A and B están separados. 

Escriba la clase Rectangulo que incluya dos datos miembro: la esquina superior izquierda y
la esquina superior derecha del Rectángulo. Cada uno de estos datos miembro son objetos de la
clase Coordenada.

El método main, debe crear la Interseccion de los Rectángulos ingresados por el usuario, mostrar la
información de cada rectángulo y mostrar el estado en el que se genera la interseccion:
Overlapped, Touching o None/Empty.
Los rectangulos deben de mostrar el area y la medida de sus areas.
Además, si los dos rectángulos se sobreponen debe mostrar el área superpuesta.

Debe asumir que el usuario ingresa dos esquinas distintas para el rectángulo. Debe asumir que el
rectángulo tiene siempre un área positiva.
Debe escribir cualquier método adicional que considere necesario. 

Salida:

**Caso 1:**
1. Ingrese una esquina del 1er rectángulo: 1, 2 
2. Ingrese la esquina opuesta del 1er rectángulo: 6, 10
3. Ingrese una esquina del 2do rectángulo: 3, 5
4. Ingrese la esquina opuesta del 2do rectángulo: 9, 13

* Rectangulo A = ((1,2), (6,10))
* Rectangulo B = ((3, 5), (9,13))
* Rectangulos A y B se sobreponen.
* Area de la intersecion =225

**Caso 2:**
1. Ingrese una esquina del 1er rectángulo: 1, 5
2. Ingrese la esquina opuesta del 1er rectángulo: 5, 9
3. Ingrese una esquina del 2do rectángulo: 5, 5
4. Ingrese la esquina opuesta del 2do rectángulo: 12, 9
* Rectangulo A = ((1, 5), (5, 9))
* Rectangulo B = ((5, 5), (12, 9))
* Rectangulos A y B se tocan
* Area de intersecion =0

**Caso 3:**
1. Ingrese una esquina del 1er rectángulo: 1,1
2. Ingrese la esquina opuesta del 1er rectángulo: 6,3
3. Ingrese una esquina del 2do rectángulo: 3,4
4. Ingrese una esquina del 2do rectángulo: 5,6
* Rectangulo A = ((1,1), (6,3h)
* Rectangulos A y B están separados
* Lado Alto de la intersecion =-1
* Lado Ancho de la intersecion =2


**Caso 4:**
1.Ingrese una esquina del 1er rectángulo: 2,3
2.Ingrese la esquina opuesta del 1er rectángulo: 9,12
3.Ingrese una esquina del 2do rectángulo: 3,4
4.Ingrese la esquina opuesta del 2do rectángulo: 7,10
* Rectangulo A = ((2,3), (9,10))
* Rectangulo B = ((3,4), (7,10))
* Rectangulos A y B se sobreponen.
* Area de intersecion =576

_based on_ 
http://www.solveet.com/exercises/Hallar-el-area-de-sobreposicion-de-dos-rectangulos-en-JAVA--con-las-clases-ya-definidas-/903/solution-4455

_how to find the intersecion of 2 rectangles__
https://www.lawebdelprogramador.com/foros/Algoritmia/315061-Interseccion-de-Rectangulos.html


